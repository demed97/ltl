package com.android.dan.ltl.ui.main.review

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.os.bundleOf
import com.android.dan.ltl.R
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import com.android.dan.ltl.ui.base.BaseFragment
import com.android.dan.ltl.ui.main.add.AddTransactionFragment
import com.android.dan.ltl.util.formatter.Formatter.DateFormatter
import kotlinx.android.synthetic.main.fragment_review_transaction.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ReviewTransactionFragment :
    BaseFragment<ReviewTransactionViewModel>(R.layout.fragment_review_transaction) {

    private val transaction by lazy {
        requireArguments().get(KEY_CURRENT_TRANSACTION) as Transaction
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.review_title)
        setAttributes(transaction)
        setHasOptionsMenu(true)
        configureFab()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.removeTransaction(transaction)
        requireActivity().onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun setAttributes(transaction: Transaction) {
        transactionNameTV.text = transaction.title
        dateTV.text = DateFormatter.getPointFormat(transaction.date)
        commentTV.text = transaction.description
        amountTV.text = when (transaction.transactionType) {
            TransactionType.INCOME -> getString(R.string.plus_s, transaction.amount)
            TransactionType.EXPENSES -> getString(R.string.minus_s, transaction.amount)
            else -> null
        }
    }

    private fun configureFab() {
        changeTransactionFAB.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
            requireActivity()
                .supportFragmentManager
                .beginTransaction()
                .replace(
                    R.id.transitionFragmentContainer,
                    AddTransactionFragment.newInstance(
                        transaction,
                        requireArguments().get(KEY_CURRENT_POSITION) as Int
                    )
                )
                .addToBackStack(null)
                .commit()
        }
    }

    companion object {
        private const val KEY_CURRENT_TRANSACTION: String = "review"
        private const val KEY_CURRENT_POSITION: String = "position"

        fun newInstance(transaction: Transaction, position: Int) =
            ReviewTransactionFragment().apply {
                arguments = bundleOf(
                    KEY_CURRENT_TRANSACTION to transaction,
                    KEY_CURRENT_POSITION to position
                )
            }
    }
}

