package com.android.dan.ltl.ui.main.list

import android.view.View
import androidx.core.content.ContextCompat
import com.android.dan.ltl.R
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import com.android.dan.ltl.ui.base.BaseAdapter
import com.android.dan.ltl.util.formatter.Formatter.DateFormatter
import kotlinx.android.synthetic.main.transaction_item.view.*

class TransactionAdapter(private val onItemClickListener: OnItemClickListener) :
    BaseAdapter<Transaction>() {

    override fun getLayoutId(viewType: Int): Int = R.layout.transaction_item

    override fun getViewHolder(view: View, viewType: Int): BaseViewHolder<Transaction> {
        return TransactionViewHolder(view, onItemClickListener)
    }

    inner class TransactionViewHolder(
        containerView: View,
        private val onItemClickListener: OnItemClickListener
    ) : BaseViewHolder<Transaction>(containerView) {

        override fun bindView(data: Transaction) {

            containerView.transactionNameTextView.text = data.title
            setDecorateAmountText(data)
            containerView.transactionDateTextView.text = DateFormatter.getPointFormat(data.date)
            containerView.setOnClickListener {
                onItemClickListener.onItemClick(data, adapterPosition)
            }
        }

        private fun setDecorateAmountText(data: Transaction) {
            when (data.transactionType) {
                TransactionType.INCOME -> {
                    val incomeText = containerView.context.getString(R.string.plus_s, data.amount)
                    val incomeColor = ContextCompat
                        .getColor(containerView.context, R.color.colorGreen)
                    decorateTextView(incomeText, incomeColor)
                }
                TransactionType.EXPENSES -> {
                    val expensesText =
                        containerView.context.getString(R.string.minus_s, data.amount)
                    val expensesColor = ContextCompat
                        .getColor(containerView.context, R.color.colorAccent)
                    decorateTextView(expensesText, expensesColor)
                }
            }
        }

        private fun decorateTextView(text: String, color: Int) {
            containerView.transactionAmountTextView.text = text
            containerView.transactionAmountTextView.setTextColor(color)
            containerView.currencyTransactionTextView.setTextColor(color)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(transaction: Transaction, position: Int)
    }
}