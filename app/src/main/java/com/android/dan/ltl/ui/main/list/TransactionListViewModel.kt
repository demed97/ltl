package com.android.dan.ltl.ui.main.list

import androidx.lifecycle.LiveData
import com.android.dan.ltl.database.repository.TransactionRepository
import com.android.dan.ltl.ui.base.BaseViewModel
import com.android.dan.ltl.util.livedata.SingleLiveEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class TransactionListViewModel(private val transactionRepository: TransactionRepository) :
    BaseViewModel() {

    private val _totalEvent = SingleLiveEvent<Int>()
    val totalEvent: LiveData<Int> = _totalEvent

    fun getTotal() {
        scope.launch {
            transactionRepository.getTotal().collect {
                _totalEvent.postValue(it)
            }
        }
    }
}