package com.android.dan.ltl.database.repository

import com.android.dan.ltl.database.dao.TransactionsDao
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

@ExperimentalCoroutinesApi
class MockTransactionRepository(private val transactionsDao: TransactionsDao) :
    TransactionRepository {

    private var itemsTransactionList = mutableListOf<Transaction>()

    private val totalStateFlow = MutableStateFlow(calculateTotalSum())

    private val allStateFlow = MutableStateFlow(filterList(TransactionType.ALL))

    private val incomeStateFlow = MutableStateFlow(filterList(TransactionType.INCOME))

    private val expensesStateFlow = MutableStateFlow(filterList(TransactionType.EXPENSES))

    private fun filterList(transactionType: TransactionType): List<Transaction> {
        return if (transactionType == TransactionType.ALL) {
            itemsTransactionList.toMutableList()
        } else {
            itemsTransactionList.filter { it.transactionType == transactionType }.toMutableList()
        }
    }

    private fun calculateTotalSum(): Int {
        return itemsTransactionList.sumBy {
            if (it.transactionType == TransactionType.INCOME) {
                it.amount.toInt()
            } else -it.amount.toInt()
        }
    }

    override suspend fun delete(transaction: Transaction) {
        transactionsDao.delete(transaction)
        itemsTransactionList.remove(transaction)
        getTotal()
        updateFlow()
    }

    override suspend fun add(transaction: Transaction) {
        transactionsDao.add(transaction)
        itemsTransactionList.add(0, transaction)
        getTotal()
        updateFlow()
    }

    override suspend fun change(transaction: Transaction, position: Int) {
        transactionsDao.update(transaction)
        itemsTransactionList[position] = transaction
        updateFlow()
        getTotal()
    }

    private fun updateFlow() {
        allStateFlow.value = filterList(TransactionType.ALL)
        incomeStateFlow.value = filterList(TransactionType.INCOME)
        expensesStateFlow.value = filterList(TransactionType.EXPENSES)
    }

    override suspend fun getAll(transactionType: TransactionType): StateFlow<List<Transaction>> {
        itemsTransactionList = transactionsDao.getAll().sortedByDescending { it.id }.toMutableList()
        updateFlow()
        getTotal()
        return when (transactionType) {
            TransactionType.ALL -> allStateFlow
            TransactionType.INCOME -> incomeStateFlow
            TransactionType.EXPENSES -> expensesStateFlow
        }
    }

    override suspend fun getTotal(): StateFlow<Int> {
        totalStateFlow.value = calculateTotalSum()
        return totalStateFlow
    }
}