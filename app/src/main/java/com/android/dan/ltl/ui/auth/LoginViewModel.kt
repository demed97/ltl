package com.android.dan.ltl.ui.auth

import androidx.lifecycle.LiveData
import com.android.dan.ltl.database.entity.Credentials
import com.android.dan.ltl.util.result.Result
import com.android.dan.ltl.database.repository.UserRepository
import com.android.dan.ltl.ui.base.BaseViewModel
import com.android.dan.ltl.util.livedata.SingleLiveEvent
import com.android.dan.ltl.util.result.Result.ExceptionResult
import com.android.dan.ltl.util.result.Result.SuccessResult
import com.android.dan.ltl.util.validation.Validation.*
import kotlinx.coroutines.launch
import kotlin.Exception

class LoginViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    private val _usernameValidationErrorEvent = SingleLiveEvent<Result>()
    val usernameValidationErrorEvent: LiveData<Result> = _usernameValidationErrorEvent

    private val _passwordValidationErrorEvent = SingleLiveEvent<Result>()
    val passwordValidationErrorEvent: LiveData<Result> = _passwordValidationErrorEvent

    private val _onAuthorizationSuccessfulEvent = SingleLiveEvent<Unit>()
    val onAuthorizationSuccessfulEvent: LiveData<Unit> = _onAuthorizationSuccessfulEvent

    private val _loginErrorEvent = SingleLiveEvent<Exception>()
    val loginErrorEvent: LiveData<Exception> = _loginErrorEvent

    fun loginUser(credentials: Credentials) {
        scope.launch {
            if (validateUsername(credentials.username) and validatePassword(credentials.password)) {
                login(credentials)
            }
        }
    }

    private suspend fun login(credentials: Credentials) {
            showProgressBar()
            when (val result = userRepository.login(credentials)) {
                is SuccessResult<*> -> {
                    _onAuthorizationSuccessfulEvent.postValue(Unit)
                }
                is ExceptionResult -> {
                    _loginErrorEvent.postValue(result.exception)
                }
            }
            hideProgressBar()
    }

    private fun validateUsername(usernameInput: String): Boolean {
        val validationResult = Login.validate(usernameInput)
        return if (validationResult == SuccessResult(Login.Error.VALIDATION_OK)) {
            true
        } else {
            _usernameValidationErrorEvent.postValue(validationResult)
            false
        }
    }

    private fun validatePassword(passwordInput: String): Boolean {
        val validationResult = Password.validate(passwordInput)
        return if (validationResult == SuccessResult(Password.Error.VALIDATION_OK)) {
            true
        } else {
            _passwordValidationErrorEvent.postValue(validationResult)
            false
        }
    }
}