package com.android.dan.ltl.ui.main.list

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.android.dan.ltl.database.entity.TransactionType

class TransactionListPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val fragmentList =
        listOf(
            TransactionRecyclerFragment.newInstance(
                TransactionType.ALL
            ),
            TransactionRecyclerFragment.newInstance(
                TransactionType.INCOME
            ),
            TransactionRecyclerFragment.newInstance(
                TransactionType.EXPENSES
            )
        )

    fun getTitle(position: Int): Int{
       return fragmentList[position].title
    }

    override fun getItemCount(): Int = TransactionType.values().size

    override fun createFragment(position: Int): Fragment {
        return (fragmentList[position])
    }
}