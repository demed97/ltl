package com.android.dan.ltl.ui.main.add

import androidx.lifecycle.LiveData
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import com.android.dan.ltl.database.repository.TransactionRepository
import com.android.dan.ltl.ui.base.BaseViewModel
import com.android.dan.ltl.util.livedata.SingleLiveEvent
import com.android.dan.ltl.util.result.Result
import com.android.dan.ltl.util.result.Result.SuccessResult
import com.android.dan.ltl.util.validation.Validation.TransactionAmount
import com.android.dan.ltl.util.validation.Validation.TransactionTitle
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class AddTransactionViewModel(private val transactionRepository: TransactionRepository) :
    BaseViewModel() {

    private val _titleValidationErrorEvent = SingleLiveEvent<Result>()
    val titleValidationErrorEvent: LiveData<Result> = _titleValidationErrorEvent

    private val _amountValidationErrorEvent = SingleLiveEvent<Result>()
    val amountValidationErrorEvent: LiveData<Result> = _amountValidationErrorEvent

    private val _typeValidationErrorEvent = SingleLiveEvent<Unit>()
    val typeValidationErrorEvent: LiveData<Unit> = _typeValidationErrorEvent

    private val _addTransactionEvent = SingleLiveEvent<Unit>()
    val addTransactionEvent: LiveData<Unit> = _addTransactionEvent

    fun addTransaction(transaction: Transaction) {
        scope.launch {
            if( validationTitle(transaction.title) and
                validationAmount(transaction.amount) and
                (transaction.transactionType != TransactionType.ALL)
            ){
                transactionRepository.add(transaction)
                _addTransactionEvent.postValue(Unit)
            }else if (transaction.transactionType == TransactionType.ALL){
                _typeValidationErrorEvent.postValue(Unit)
            }
        }
    }

    fun changeTransaction(transaction: Transaction, position: Int) {
        scope.launch {
            if( validationTitle(transaction.title) and
                validationAmount(transaction.amount) and
                (transaction.transactionType != TransactionType.ALL)
            ){
                transactionRepository.change(transaction, position)
                _addTransactionEvent.postValue(Unit)
            }else if (transaction.transactionType == TransactionType.ALL){
                _typeValidationErrorEvent.postValue(Unit)
            }
        }
    }

    fun removeTransaction(transaction: Transaction) {
        scope.launch {
            transactionRepository.delete(transaction)
        }
    }

    private fun validationTitle(title: String): Boolean {
        val validationResult = TransactionTitle.validate(title)
        return if (validationResult == SuccessResult(TransactionTitle.Error.VALIDATION_OK)) {
            true
        } else {
            _titleValidationErrorEvent.postValue(validationResult)
            false
        }
    }

    private fun validationAmount(amount: String): Boolean {
        val validationResult = TransactionAmount.validate(amount)
        return if (validationResult == SuccessResult(TransactionAmount.Error.VALIDATION_OK)) {
            true
        } else {
            _amountValidationErrorEvent.postValue(validationResult)
            false
        }
    }
}