package com.android.dan.ltl.ui.splash

import androidx.lifecycle.LiveData
import com.android.dan.ltl.database.repository.UserRepository
import com.android.dan.ltl.ui.base.BaseViewModel
import com.android.dan.ltl.util.livedata.SingleLiveEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    private val _userIsEmptyEvent = SingleLiveEvent<Unit>()
    val userIsEmptyEvent: LiveData<Unit> = _userIsEmptyEvent

    private val _userIsAuthorizedEvent = SingleLiveEvent<Unit>()
    val userIsAuthorizedEvent: LiveData<Unit> = _userIsAuthorizedEvent

    init {
        loadIsAuthorized()
    }

    private fun loadIsAuthorized() {
        scope.launch {
            val durationJob = scope.launch {
                delay(SPLASH_DURATION)
            }
            val dataJob = scope.async {
                userRepository.isAuthorized()
            }

            durationJob.join()
            if (!dataJob.await()) {
                _userIsEmptyEvent.postValue(Unit)
            } else {
                _userIsAuthorizedEvent.postValue(Unit)
            }
        }
    }

    companion object {
       private const val SPLASH_DURATION  = 2000L
    }
}