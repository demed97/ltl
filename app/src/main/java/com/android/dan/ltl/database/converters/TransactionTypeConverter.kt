package com.android.dan.ltl.database.converters

import androidx.room.TypeConverter
import com.android.dan.ltl.database.entity.TransactionType

class TransactionTypeConverter {

    @TypeConverter
    fun stringToType(value: String?): TransactionType{
        return when(value){
            "income" -> TransactionType.INCOME
            "expenses" -> TransactionType.EXPENSES
            else -> TransactionType.ALL
        }
    }

    @TypeConverter
    fun typeToString(type: TransactionType): String? {
        return when(type){
            TransactionType.ALL -> null
            TransactionType.INCOME -> "income"
            TransactionType.EXPENSES -> "expenses"
        }
    }

}