package com.android.dan.ltl.ui.auth

import android.os.Bundle
import com.android.dan.ltl.R
import com.android.dan.ltl.ui.base.BaseActivity

class AuthActivity : BaseActivity<AuthViewModel>(R.layout.activity_auth) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showLoginFragment()
    }

    private fun showLoginFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.authFragmentContainer, LoginFragment.newInstance())
            .commit()
    }
}