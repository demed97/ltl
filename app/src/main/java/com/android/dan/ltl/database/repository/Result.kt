package com.android.dan.ltl.database.repository

sealed class Result {
    data class SuccessResult<T>(val result: T) : Result()
    data class ExceptionResult(val exception: Exception) : Result()
}