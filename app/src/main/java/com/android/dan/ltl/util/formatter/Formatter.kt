package com.android.dan.ltl.util.formatter

import java.text.SimpleDateFormat
import java.util.*

sealed class Formatter {

    object DateFormatter {

        private val pointFormat = SimpleDateFormat("dd.MM.yyyy")
        fun getPointFormat(date: Date): String = pointFormat.format(date)
    }

}