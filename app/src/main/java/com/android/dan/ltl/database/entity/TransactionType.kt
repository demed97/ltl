package com.android.dan.ltl.database.entity

enum class TransactionType {
    ALL,
    INCOME,
    EXPENSES
}