package com.android.dan.ltl.ui.main.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import com.android.dan.ltl.database.repository.TransactionRepository
import com.android.dan.ltl.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class TransactionRecyclerViewModel(private val transactionRepository: TransactionRepository) :
    BaseViewModel() {

    private val _transactionsItemLiveData = MutableLiveData<List<Transaction>>()
    val transactionsItemLiveData: LiveData<List<Transaction>> = _transactionsItemLiveData

    fun loadTransactions(transactionType: TransactionType) {
        scope.launch {
            transactionRepository.getAll(transactionType).collect {
                _transactionsItemLiveData.postValue(it)
            }
        }
    }

    fun removeTransaction(transaction: Transaction) {
        scope.launch {
            transactionRepository.delete(transaction)
        }
    }
}
