package com.android.dan.ltl.util.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.android.dan.ltl.Application
import com.android.dan.ltl.database.AppDatabase
import com.android.dan.ltl.database.repository.MockTransactionRepository
import com.android.dan.ltl.database.repository.MockUserRepository
import com.android.dan.ltl.database.repository.TransactionRepository
import com.android.dan.ltl.database.repository.UserRepository
import com.android.dan.ltl.ui.auth.AuthViewModel
import com.android.dan.ltl.ui.auth.LoginViewModel
import com.android.dan.ltl.ui.main.MainViewModel
import com.android.dan.ltl.ui.main.add.AddTransactionViewModel
import com.android.dan.ltl.ui.main.list.TransactionListViewModel
import com.android.dan.ltl.ui.main.list.TransactionRecyclerViewModel
import com.android.dan.ltl.ui.main.review.ReviewTransactionViewModel
import com.android.dan.ltl.ui.splash.SplashViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ViewModelFactory(application: Application) : ViewModelProvider.Factory {

    private val database by lazy {
        Room.databaseBuilder(application, AppDatabase::class.java, "database").build()
    }

    private val userRepository: UserRepository by lazy {
        MockUserRepository()
    }

    private val transactionRepository: TransactionRepository by lazy {
        MockTransactionRepository(database.transactionDao())
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            SplashViewModel::class.java -> SplashViewModel(userRepository)
            AuthViewModel::class.java -> AuthViewModel()
            LoginViewModel::class.java -> LoginViewModel(userRepository)
            MainViewModel::class.java -> MainViewModel()
            TransactionListViewModel::class.java -> TransactionListViewModel(transactionRepository)
            TransactionRecyclerViewModel::class.java -> TransactionRecyclerViewModel(
                transactionRepository
            )
            AddTransactionViewModel::class.java -> AddTransactionViewModel(transactionRepository)
            ReviewTransactionViewModel::class.java -> ReviewTransactionViewModel(
                transactionRepository
            )
            else -> throw IllegalStateException("IllegalStateException ${modelClass::class.java.name}")
        } as T
    }
}