package com.android.dan.ltl.ui.splash

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.android.dan.ltl.R
import com.android.dan.ltl.ui.auth.AuthActivity
import com.android.dan.ltl.ui.base.BaseActivity
import com.android.dan.ltl.ui.main.MainActivity
import com.android.dan.ltl.util.livedata.observer
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : BaseActivity<SplashViewModel>(R.layout.activity_splash_screen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashImageView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in))
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.userIsEmptyEvent.observer(this) {
            startActivityWithTransition()
        }
        viewModel.userIsAuthorizedEvent.observer(this) {
            startActivity(MainActivity::class.java)
        }
    }

    private fun startActivityWithTransition() {
        val bundle = ActivityOptions.makeSceneTransitionAnimation(
            this,
            splashImageView,
            getString(R.string.splash_transition)
        ).toBundle()
        startActivity(AuthActivity::class.java, bundle)
    }

    private fun <T> startActivity(transmittedClass: Class<T>, bundle: Bundle? = null) {
        startActivity(Intent(this, transmittedClass), bundle)
        finish()
    }
}