package com.android.dan.ltl.util.result

sealed class Result {
    data class SuccessResult<T>(val result: T) : Result()
    data class ExceptionResult(val exception: Exception) : Result()
}