package com.android.dan.ltl.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.android.dan.ltl.util.context.getViewModelProvider
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM : BaseViewModel>(
    @LayoutRes contentLayoutId: Int
) : AppCompatActivity(contentLayoutId) {

    protected val viewModel: VM by lazy {
        getViewModelProvider(this)
            .get((javaClass.genericSuperclass as ParameterizedType)
                    .actualTypeArguments[0] as Class<VM>)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribe()
    }

   @CallSuper protected open fun subscribe() {}
}