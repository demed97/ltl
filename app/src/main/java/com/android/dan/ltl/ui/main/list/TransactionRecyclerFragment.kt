package com.android.dan.ltl.ui.main.list

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.dan.ltl.R
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import com.android.dan.ltl.ui.base.BaseFragment
import com.android.dan.ltl.ui.main.list.TransactionAdapter.OnItemClickListener
import com.android.dan.ltl.ui.main.review.ReviewTransactionFragment
import com.android.dan.ltl.util.livedata.observer
import kotlinx.android.synthetic.main.fragment_transaction_recycler.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class TransactionRecyclerFragment :
    BaseFragment<TransactionRecyclerViewModel>(R.layout.fragment_transaction_recycler),
    OnItemClickListener {

    private val adapter = TransactionAdapter(this)

    val title by lazy {
        when (requireArguments().get(KEY_TRANSACTION_PAGE)) {
            TransactionType.INCOME -> R.string.income
            TransactionType.EXPENSES -> R.string.expenses
            else -> R.string.all_page_title
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel.loadTransactions(requireArguments().get(KEY_TRANSACTION_PAGE) as TransactionType)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.transactionsItemLiveData.observer(this) {
            adapter.setItems(it)
        }
    }

    override fun onItemClick(transaction: Transaction, position: Int) {
        requireActivity()
            .supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.transitionFragmentContainer,
                ReviewTransactionFragment.newInstance(transaction, position)
            )
            .addToBackStack(null)
            .commit()

    }

    private fun initRecyclerView() {
        transactionRecyclerView.adapter = adapter
        val swipeHandler = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.removeTransaction(adapter.getItem(viewHolder.adapterPosition))
                adapter.removeItem(viewHolder.adapterPosition)
            }
        }
        ItemTouchHelper(swipeHandler).attachToRecyclerView(transactionRecyclerView)
    }

    companion object {

        private const val KEY_TRANSACTION_PAGE = "transaction_page"

        fun newInstance(transactionType: TransactionType) =
            TransactionRecyclerFragment().apply {
                arguments = bundleOf(KEY_TRANSACTION_PAGE to transactionType)
            }
    }
}