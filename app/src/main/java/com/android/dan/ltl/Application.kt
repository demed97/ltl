package com.android.dan.ltl

import android.app.Application
import com.android.dan.ltl.util.viewmodel.ViewModelFactory

class Application : Application() {

    val viewModelFactory = ViewModelFactory(this)
}