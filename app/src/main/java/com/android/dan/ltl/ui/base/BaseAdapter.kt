package com.android.dan.ltl.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseAdapter<T>.BaseViewHolder<T>>() {

    private val listItems = mutableListOf<T>()

    fun setItems(listItems: Collection<T>) {
        this.listItems.clear()
        this.listItems.addAll(listItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val inflater = LayoutInflater.from(parent.context)
        val layoutId = getLayoutId(viewType)
        val view = inflater.inflate(layoutId, parent, false)
        return getViewHolder(view, viewType)
    }

    @LayoutRes
    protected abstract fun getLayoutId(viewType: Int): Int

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bindView(listItems[position])
    }

    override fun getItemCount(): Int = listItems.size

    abstract fun getViewHolder(view: View, viewType: Int): BaseViewHolder<T>

    fun addItem(item: T, position: Int = 0) {
        listItems.add(position, item)
        notifyItemInserted(position)
    }

    fun removeItem(position: Int){
        if (position < listItems.size){
            listItems.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): T {
        return listItems[position]
    }

    abstract inner class BaseViewHolder<T>(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        abstract fun bindView(data: T)
    }
}