package com.android.dan.ltl.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.android.dan.ltl.R
import com.android.dan.ltl.database.entity.Credentials
import com.android.dan.ltl.ui.base.BaseFragment
import com.android.dan.ltl.ui.main.MainActivity
import com.android.dan.ltl.util.livedata.observer
import com.android.dan.ltl.util.result.Result
import com.android.dan.ltl.util.result.Result.SuccessResult
import com.android.dan.ltl.util.validation.Validation.Login
import com.android.dan.ltl.util.validation.Validation.Password
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment<LoginViewModel>(R.layout.fragment_login) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFocusChangeListener(usernameTil, textInputUsername)
        setupFocusChangeListener(passwordTil, textInputPassword)
        loginButton.setOnClickListener {
            onLoginClicked()
        }
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.usernameValidationErrorEvent.observer(this) {
            validationResult(it, textInputUsername)
        }
        viewModel.passwordValidationErrorEvent.observer(this) {
            validationResult(it, textInputPassword)
        }
        viewModel.onAuthorizationSuccessfulEvent.observer(this) {
            startMainActivity()
        }
        viewModel.loginErrorEvent.observer(this) {
            showSnackbar(it)
        }
    }

    private fun setupFocusChangeListener(
        textInputEditText: TextInputEditText?,
        textInputLayout: TextInputLayout?
    ) {
        textInputEditText?.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                textInputLayout?.error = null
            }
        }
    }

    private fun showSnackbar(exception: Exception) {
        Snackbar.make(
            loginConstLayout,
            getString(R.string.bad_credential_auth_message),
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction(getString(R.string.snackbar_ok), View.OnClickListener { })
            .show()
    }

    private fun startMainActivity() {
        startActivity(Intent(context, MainActivity::class.java))
        requireActivity().finish()
    }

    private fun onLoginClicked() {
        viewModel.loginUser(
            Credentials(
                textInputUsername.editText?.text.toString(),
                textInputPassword.editText?.text.toString()
            )
        )
    }

    override fun validationResult(validateErrors: Result, textInputLayout: TextInputLayout?) {
        super.validationResult(validateErrors, textInputLayout)
        val errorMessage: String? = when (validateErrors) {
            SuccessResult(Login.Error.USERNAME_EMPTY) -> getString(R.string.username_empty)
            SuccessResult(Login.Error.USERNAME_IS_TOO_SHORT) -> getString(R.string.username_less_2)
            SuccessResult(Login.Error.USERNAME_IS_TOO_LONG) -> getString(R.string.username_more_15)
            SuccessResult(Password.Error.PASSWORD_EMPTY) -> getString(R.string.password_empty)
            SuccessResult(Password.Error.PASSWORD_IS_TOO_LONG) -> getString(R.string.password_more_15)
            SuccessResult(Password.Error.PASSWORD_IS_TOO_SHORT) -> getString(R.string.password_less_5)
            SuccessResult(Login.Error.USERNAME_REGEX) -> getString(R.string.username_regex)
            SuccessResult(Password.Error.PASSWORD_REGEX) -> getString(R.string.password_regex)
            else -> null
        }
        showError(textInputLayout, errorMessage)
    }

    companion object {

        fun newInstance() = LoginFragment()
    }
}