package com.android.dan.ltl.database.entity

data class Credentials(val username: String, val password: String) {}