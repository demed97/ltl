package com.android.dan.ltl.database.repository

import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.StateFlow

@ExperimentalCoroutinesApi
interface TransactionRepository {

    suspend fun getAll(transactionType: TransactionType): StateFlow<List<Transaction>>

    suspend fun getTotal(): StateFlow<Int>

    suspend fun delete(transaction: Transaction)

    suspend fun add(transaction: Transaction)

    suspend fun change(transaction: Transaction, position: Int)
}