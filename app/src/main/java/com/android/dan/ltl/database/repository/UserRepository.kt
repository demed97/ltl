package com.android.dan.ltl.database.repository

import com.android.dan.ltl.database.entity.Credentials
import com.android.dan.ltl.util.result.Result

interface UserRepository {

    suspend fun isAuthorized(): Boolean

    suspend fun login(credentials: Credentials): Result
}