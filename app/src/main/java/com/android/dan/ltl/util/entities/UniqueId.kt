package com.android.dan.ltl.util.entities

interface UniqueId {
    fun getUniqueId(): Long
}