package com.android.dan.ltl.ui.main.add

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import com.android.dan.ltl.R
import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.entity.TransactionType
import com.android.dan.ltl.ui.base.BaseFragment
import com.android.dan.ltl.util.formatter.Formatter.DateFormatter
import com.android.dan.ltl.util.livedata.observer
import com.android.dan.ltl.util.result.Result
import com.android.dan.ltl.util.result.Result.SuccessResult
import com.android.dan.ltl.util.validation.Validation.TransactionAmount
import com.android.dan.ltl.util.validation.Validation.TransactionTitle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_add_transaction.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*

@ExperimentalCoroutinesApi
class AddTransactionFragment :
    BaseFragment<AddTransactionViewModel>(R.layout.fragment_add_transaction) {

    private var transactionType: TransactionType = TransactionType.ALL
    private lateinit var date: Date
    private lateinit var transaction: Transaction

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTypeButtonsListener()

        if (requireArguments().get(KEY_CHANGE_FRAGMENT) == null) {
            requireActivity().title = getString(R.string.add_title)
            date = Date()
            checkTransactionFAB.setOnClickListener { onCheckClicked() }
        } else {
            transaction = requireArguments().get(KEY_CHANGE_FRAGMENT) as Transaction
            date = transaction.date
            requireActivity().title = getString(R.string.change_title)

            filInFields(transaction)
            setHasOptionsMenu(true)
            checkTransactionFAB.setOnClickListener {
                onChangeClicked(requireArguments().get(KEY_CURRENT_POSITION) as Int)
            }
        }

        setTextInputDate()
        setupFocusChangeListener(transactionNameTil, textInputTransactionName)
        setupFocusChangeListener(amountTil, textInputAmount)
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.addTransactionEvent.observer(this) {
            requireActivity().supportFragmentManager.beginTransaction().remove(this).commit()
            requireActivity().onBackPressed()
        }
        viewModel.titleValidationErrorEvent.observer(this) {
            validationResult(it, textInputTransactionName)
        }
        viewModel.amountValidationErrorEvent.observer(this) {
            validationResult(it, textInputAmount)
        }
        viewModel.typeValidationErrorEvent.observer(this) {
            showSnackbar()
        }
    }

    override fun validationResult(validateErrors: Result, textInputLayout: TextInputLayout?) {
        super.validationResult(validateErrors, textInputLayout)
        val errorMessage: String? = when (validateErrors) {
            SuccessResult(TransactionTitle.Error.TITLE_EMPTY) -> getString(R.string.empty_title)
            SuccessResult(TransactionAmount.Error.AMOUNT_EMPTY) -> getString(R.string.empty_amount)
            SuccessResult(TransactionAmount.Error.AMOUNT_IS_TOO_LONG) ->
                getString(R.string.amount_more_10)
            SuccessResult(TransactionAmount.Error.AMOUNT_FIRST_NULL) ->
                getString(R.string.amount_first_null)
            else -> null
        }
        showError(textInputLayout, errorMessage)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.removeTransaction(transaction)
        requireActivity().onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun filInFields(transaction: Transaction) {
        transactionNameTil.setText(transaction.title)
        amountTil.setText(transaction.amount)
        commentTil.setText(transaction.description)
        selectTypeButton(transaction.transactionType)
    }

    private fun selectTypeButton(transactionType: TransactionType) {
        if (transactionType == TransactionType.INCOME){
            incomeButton.performClick()
        } else expensesButton.performClick()

    }

    private fun showSnackbar() {
        Snackbar.make(
            addConstLayout,
            getString(R.string.select_type_transaction),
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction(getString(R.string.snackbar_ok), View.OnClickListener { })
            .show()
    }

    private fun setTypeButtonsListener() {
        val colorPrimary = ContextCompat.getColor(requireContext(), R.color.colorPrimary)
        val background = R.drawable.button_border
        val colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        val colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        incomeButton.setOnClickListener {
            incomeButton.setBackgroundColor(colorPrimary)
            incomeButton.setTextColor(colorWhite)
            expensesButton.setBackgroundResource(background)
            expensesButton.setTextColor(colorBlack)
            transactionType = TransactionType.INCOME
        }
        expensesButton.setOnClickListener {
            expensesButton.setBackgroundColor(colorPrimary)
            expensesButton.setTextColor(colorWhite)
            incomeButton.setBackgroundResource(background)
            incomeButton.setTextColor(colorBlack)
            transactionType = TransactionType.EXPENSES
        }
    }

    private fun setupFocusChangeListener(
        textInputEditText: TextInputEditText?,
        textInputLayout: TextInputLayout?
    ) {
        textInputEditText?.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                textInputLayout?.error = null
            }
        }
    }

    private fun onCheckClicked() {
        viewModel.addTransaction(
            Transaction(
                textInputTransactionName.editText?.text.toString(),
                transactionType,
                textInputAmount.editText?.text.toString(),
                date,
                textInputComment.editText?.text.toString()
            )
        )
    }

    private fun onChangeClicked(position: Int) {
        viewModel.changeTransaction(
            Transaction(
                textInputTransactionName.editText?.text.toString(),
                transactionType,
                textInputAmount.editText?.text.toString(),
                date,
                textInputComment.editText?.text.toString()
            ), position
        )
    }

    private fun setTextInputDate() {
        textInputDate.text = DateFormatter.getPointFormat(date)
        textInputDate.setOnClickListener { callDatePicker() }
    }

    private fun callDatePicker() {

        val cal = Calendar.getInstance()

        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                date = cal.time
                textInputDate.text = DateFormatter.getPointFormat(date)
            }

        DatePickerDialog(
            requireActivity(), dateSetListener,
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        ).show()
    }


    companion object {

        private const val KEY_CHANGE_FRAGMENT: String = "change"
        private const val KEY_CURRENT_POSITION: String = "change_position"

        fun newInstance() =
            AddTransactionFragment().apply {
                arguments = bundleOf(KEY_CHANGE_FRAGMENT to null)
            }

        fun newInstance(transaction: Transaction, position: Int) =
            newInstance().apply {
                arguments = bundleOf(
                    KEY_CHANGE_FRAGMENT to transaction,
                    KEY_CURRENT_POSITION to position
                )
            }
    }
}

