package com.android.dan.ltl.database.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.android.dan.ltl.database.converters.DateConverter
import com.android.dan.ltl.database.converters.TransactionTypeConverter
import com.android.dan.ltl.util.entities.UniqueId
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity
data class Transaction(
    val title: String,
    @TypeConverters(TransactionTypeConverter::class)
    val transactionType: TransactionType,
    val amount: String,
    @TypeConverters(DateConverter::class)
    val date: Date,
    val description: String
) : Parcelable, UniqueId {

    @PrimaryKey(autoGenerate = true) var id: Long? = null

    override fun getUniqueId(): Long = id!!
}