package com.android.dan.ltl.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.android.dan.ltl.database.converters.DateConverter
import com.android.dan.ltl.database.converters.TransactionTypeConverter
import com.android.dan.ltl.database.dao.TransactionsDao
import com.android.dan.ltl.database.entity.Transaction

@Database(entities = [Transaction::class], version = 1, exportSchema = false)
@TypeConverters(TransactionTypeConverter::class, DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun transactionDao(): TransactionsDao
}