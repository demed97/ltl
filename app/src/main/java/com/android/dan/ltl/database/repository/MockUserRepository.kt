package com.android.dan.ltl.database.repository

import com.android.dan.ltl.database.entity.Credentials
import com.android.dan.ltl.util.result.Result
import kotlinx.coroutines.delay

class MockUserRepository : UserRepository {

    private val credentials = Credentials("demed", "dD1234")
    private var isAuthorized = true

    override suspend fun isAuthorized(): Boolean {
        return isAuthorized
    }

    override suspend fun login(credentials: Credentials): Result {
        delay(1000)
        return if (this.credentials == credentials) {
            isAuthorized = true
            Result.SuccessResult<Boolean>(true)
        } else {
            Result.ExceptionResult(BadCredentialsException())
        }
    }
}
