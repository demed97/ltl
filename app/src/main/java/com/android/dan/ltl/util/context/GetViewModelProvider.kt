package com.android.dan.ltl.util.context

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.android.dan.ltl.Application

fun Context.getViewModelProvider(owner: ViewModelStoreOwner): ViewModelProvider {
    val viewModelFactory = (applicationContext as Application).viewModelFactory
    return ViewModelProvider(owner, viewModelFactory)
}
