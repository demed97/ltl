package com.android.dan.ltl.ui.base

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.android.dan.ltl.ui.dialog.ProgressDialogFragment
import com.android.dan.ltl.util.context.getViewModelProvider
import com.android.dan.ltl.util.livedata.observer
import com.android.dan.ltl.util.result.Result
import com.google.android.material.textfield.TextInputLayout
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<VM : BaseViewModel>(
    @LayoutRes contentLayoutId: Int
) : Fragment(contentLayoutId) {

    protected val viewModel: VM by lazy {
        requireContext()
            .getViewModelProvider(this)
            .get((javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<VM>)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe()
    }

    fun showProgressBar() = ProgressDialogFragment.show(childFragmentManager)

    fun hideProgressBar() = ProgressDialogFragment.dismiss(childFragmentManager)

    protected open fun validationResult(validateErrors: Result, textInputLayout: TextInputLayout?){}

    protected fun showError(textInputLayout: TextInputLayout?, errorMessage: String?) {
        textInputLayout?.error = errorMessage
    }

    @CallSuper protected open fun subscribe() {
        viewModel.visibleProgressBarLiveData.observer(this){
            if (it) showProgressBar() else hideProgressBar()
        }
    }
}