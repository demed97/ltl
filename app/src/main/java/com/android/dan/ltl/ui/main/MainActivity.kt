package com.android.dan.ltl.ui.main

import android.os.Bundle
import android.view.MenuItem
import com.android.dan.ltl.R
import com.android.dan.ltl.ui.base.BaseActivity
import com.android.dan.ltl.ui.main.list.TransactionListFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MainActivity : BaseActivity<MainViewModel>(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.elevation = 0f
        showTransactionListFragment()
        backStackChangedListener()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(supportFragmentManager.backStackEntryCount < 1){
            setVisibilityBackButton(false)
        }
    }

    private fun backStackChangedListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount > 0) {
                setVisibilityBackButton(true)
            }
        }
    }

    private fun setVisibilityBackButton(visibility: Boolean) {
        supportActionBar?.setHomeButtonEnabled(visibility)
        supportActionBar?.setDisplayHomeAsUpEnabled(visibility)
    }

    private fun showTransactionListFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.transitionFragmentContainer, TransactionListFragment.newInstance())
            .commit()
    }
}