package com.android.dan.ltl.util.validation

import com.android.dan.ltl.util.result.Result
import com.android.dan.ltl.util.result.Result.SuccessResult

sealed class Validation {

    abstract fun validate(validated: String): Result

    object Login : Validation() {

        override fun validate(validated: String): SuccessResult<Error> {
            return when {
                validated.isEmpty() -> SuccessResult(Error.USERNAME_EMPTY)
                validated.length < MIN_USERNAME_LENGTH ->
                    SuccessResult(Error.USERNAME_IS_TOO_SHORT)

                validated.length > MAX_AVAILABLE_INPUT_LINE_LENGTH ->
                    SuccessResult(Error.USERNAME_IS_TOO_LONG)

                USERNAME_REGEX.matchEntire(validated) == null ->
                    SuccessResult(Error.USERNAME_REGEX)

                else -> SuccessResult(Error.VALIDATION_OK)
            }
        }

        enum class Error {
            USERNAME_EMPTY,
            USERNAME_IS_TOO_LONG,
            USERNAME_IS_TOO_SHORT,
            USERNAME_REGEX,
            VALIDATION_OK
        }
    }

    object Password : Validation() {

        override fun validate(validated: String): SuccessResult<Error> {
            return when {
                validated.isEmpty() -> SuccessResult(Error.PASSWORD_EMPTY)
                validated.length < MIN_PASSWORD_LENGTH ->
                    SuccessResult(Error.PASSWORD_IS_TOO_SHORT)

                validated.length > MAX_AVAILABLE_INPUT_LINE_LENGTH ->
                    SuccessResult(Error.PASSWORD_IS_TOO_LONG)

                PASSWORD_REGEX.matchEntire(validated) == null ->
                    SuccessResult(Error.PASSWORD_REGEX)

                else -> SuccessResult(Error.VALIDATION_OK)
            }
        }

        enum class Error {
            PASSWORD_EMPTY,
            PASSWORD_IS_TOO_SHORT,
            PASSWORD_IS_TOO_LONG,
            PASSWORD_REGEX,
            VALIDATION_OK
        }
    }

    object TransactionTitle : Validation() {

        override fun validate(validated: String): SuccessResult<Error> {
            return when {
                validated.isEmpty() -> SuccessResult(Error.TITLE_EMPTY)
                else -> SuccessResult(Error.VALIDATION_OK)
            }
        }

        enum class Error {
            TITLE_EMPTY,
            VALIDATION_OK
        }
    }

    object TransactionAmount : Validation() {

        override fun validate(validated: String): SuccessResult<Error> {
            return when {
                validated.isEmpty() -> SuccessResult(Error.AMOUNT_EMPTY)
                validated[0] == '0' -> SuccessResult(Error.AMOUNT_FIRST_NULL)
                validated.length > MAX_AMOUNT_LINE_LENGTH -> SuccessResult(Error.AMOUNT_IS_TOO_LONG)
                else -> SuccessResult(Error.VALIDATION_OK)
            }
        }

        enum class Error {
            AMOUNT_EMPTY,
            AMOUNT_FIRST_NULL,
            AMOUNT_IS_TOO_LONG,
            VALIDATION_OK
        }
    }



    companion object {
        private const val MAX_AVAILABLE_INPUT_LINE_LENGTH = 15
        private const val MAX_AMOUNT_LINE_LENGTH = 10
        private const val MIN_USERNAME_LENGTH = 3
        private val USERNAME_REGEX = Regex("^[a-zA-Z][a-zA-Z0-9-_\\.]{2,16}\$")
        private const val MIN_PASSWORD_LENGTH = 6
        private val PASSWORD_REGEX = Regex("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*\$")
    }
}