package com.android.dan.ltl.ui.main.list

import android.os.Bundle
import android.view.View
import com.android.dan.ltl.R
import com.android.dan.ltl.ui.base.BaseFragment
import com.android.dan.ltl.ui.main.add.AddTransactionFragment
import com.android.dan.ltl.util.livedata.observer
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_transaction_list.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class TransactionListFragment :
    BaseFragment<TransactionListViewModel>(R.layout.fragment_transaction_list) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureViewPager()
        viewModel.getTotal()
        configureFab()
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.totalEvent.observer(this){
            requireActivity().title = getString(R.string.balance_title, it.toString())
        }
    }

    private fun configureFab() {
        addTransactionFAB.setOnClickListener {
            requireActivity()
                .supportFragmentManager
                .beginTransaction()
                .replace(R.id.transitionFragmentContainer, AddTransactionFragment.newInstance())
                .addToBackStack(null)
                .commit()
        }
    }

    private fun configureViewPager() {
        val adapter =
            TransactionListPagerAdapter(this)
        transactionViewPager.adapter = adapter
        TabLayoutMediator(transactionTabLayout, transactionViewPager) { tab, position ->
            tab.text = getString(adapter.getTitle(position))
        }.attach()
        transactionViewPager.isUserInputEnabled = false
    }

    companion object {

        fun newInstance() = TransactionListFragment()
    }
}