package com.android.dan.ltl.ui.main.review

import com.android.dan.ltl.database.entity.Transaction
import com.android.dan.ltl.database.repository.TransactionRepository
import com.android.dan.ltl.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class ReviewTransactionViewModel(private val transactionRepository: TransactionRepository) :
    BaseViewModel() {

    fun removeTransaction(transaction: Transaction) {
        scope.launch {
            transactionRepository.delete(transaction)
        }
    }
}